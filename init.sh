#!/bin/bash

export WORK="/home/work"

#setting maven initial values


echo "--- Configuration ---"
java -version
gradle -v
env
echo "--- End Configuration ---"
echo "--- Starting VNC ---"
export DISPLAY=:1
#start-stop-daemon --start --quiet --pidfile $PIDFILE --make-pidfile --background --exec /usr/bin/Xvfb -- :1 -screen 0 1024x768x24 -ac +extension GLX +render -noreset
echo "Starting Xvfb on Display :0 with a resolution of 1024x768x16"
Xvfb :1 -screen 0 1024x768x16 &
echo "--- END VNC ---"

#exec "$@"
exec bash
