# JavaDevDocker

This project aims to provide a docker image for continuous integration of java development. It is intended to use with GitLab CI, but can be used for other purposes too.

## Environment
For development the user `work` is used with the home directory `/home/work`. The home directory has the following pre installed directories:
* localLibs - holding external jars that are required for build
* localMaven - the local maven repository

For development purposes, these directories may be mounted from the local drive:  
`docker run -i -v /Users/<name>/JavaDevDocker/mntDev/:/home/work java-dev`

To identify the execution environment, the environment variable `IS_CONTAINER=true` is set. This variable may be used e.g. to adjust timing in performance sensitive tests (UI animations etc.).

### Boot sequence
When docker is startet the init script is executed from `/home/script/init.sh`. The init script currenty prints out some version information and starts the virtual frame buffer.

### Maven
The local maven repository is set to `/home/work/localMaven/`.
### gradle
Gradle is installed for builds. Gradle is set up to look for libraries in `localLibs` and `localMaven`. Publish to mavenLocal will publish the artifacts to `localMaven`. 
You should use these repository definitions for that matter:

```groovy
maven {
			url "file://$LOCAL_MAVEN"
			name 'localMaven'
		}
		flatDir {
			dirs "$LOCAL_JAR"
			name 'localJARs'
		}
```

### Xvfb
For UI-Testing a virtual framebuffer is installed and running on `DISPLAY=:1`. Everything is set up, so you should be able to start your application/test right after the boot up.

## Installed packages
* Java 
   * Oracle JDK 8
* Development Tools
   * Gradle (2.10)
   * Maven
   * Git
* xvfb - for graphics output (and required packages)

## Change Notes
### 0.1 
Initial implementation
Features:
* Oracle Java 1.8
* Gradle
* Maven
* User Environment set up

### 0.2
* Added Frame Buffer (#4)
* Added environment variable `IS_CONTAINER=true`

### 0.4
 * Upgraded Java: `1.8.0_121-b13`
 * Upgraded Gradle: `2.10`
 * Stripped down size of image a bit.
 
### 0.5
 * Added german and us utf-8 locale