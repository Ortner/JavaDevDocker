## Change Log
### 0.1 
Initial implementation
Features:
* Oracle Java 1.8
* Gradle
* Maven
* User Environment set up

### 0.2
* Added Frame Buffer (#4)
* Added environment variable `IS_CONTAINER=true`

### 0.4
 * Upgraded Java: `1.8.0_121-b13`
 * Upgraded Gradle: `2.10`
 * Stripped down size of image a bit.
 
### 0.5
 * Added german and us utf-8 locale
